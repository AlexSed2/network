public struct App {
    public init() {}
    public func run() async {
        await testIterator()
//        try! await Fetcher().run()
//        try! await Explorer().runAsync()
    }
    func testIterator() async {
        do {
            var dict: [Int: [BadIcon]] = [:]
            let automatic = try await Automatic(eachLenth: 25)
            for try await icon in automatic {
                if icon.id % 10 == 0 {
                    await Task.sleep(500_000_000)
                }
                print("icon: \(icon)")
                if dict[icon.id] != nil {
                    fatalError()
                } else {
                    dict[icon.id] = [icon]
                }
            }
        } catch {
            print("no automatic")
        }
    }
}
