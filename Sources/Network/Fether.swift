import Foundation
import Files

public struct Fetcher {
    public init() {}
    
    let foldername = "Albums"
    
    var folder: Folder {
        get { Folders().getLocaleFolder(foldername: foldername)! }
    }
    
    public func run() async throws {
        let shapes = try await GetAlbumsShape().runAsync()
        for shape in shapes {
            let badAlbum: BadAlbum
            if let badalbum = await readBadAlbum(album: shape) {
                badAlbum = badalbum
            } else {
                badAlbum = try await GetOneBadAlbum().runAsync(number: shape.id)
                try await save(badAlbum: badAlbum)
            }
            print("album: \(badAlbum.title), \(badAlbum.icons.count)")
        }
    }
    
    public func readAllBadAlbumsSync() -> [BadAlbum] {
        var albums = [BadAlbum]()
        for file in folder.files {
            let data = try! file.read()
            let model = try! JSONDecoder().decode(BadAlbum.self, from: data)
            albums.append(model)
        }
        return albums
    }
    
    func readBadAlbum(album: Album) async -> BadAlbum? {
        guard let file = try? folder.file(named: album.name) else { return nil }
        guard let data = try? file.read() else { return nil }
        let model = try? JSONDecoder().decode(BadAlbum.self, from: data)
        return model
    }
    
    func save(badAlbum: BadAlbum) async throws {
        let file = try folder.createFile(named: badAlbum.title)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(badAlbum)
        try! file.write(data)
    }
    
    public func fetch() async throws -> [BadAlbum] {
        if let model = SaveAndRead().read() {
            return model
        } else {
            let model = try await fetchFromInternet()
            SaveAndRead().save(model: model)
            return model
        }
    }
    public func fetchFromInternet() async throws -> [BadAlbum] {
        let shapes = try await GetAlbumsShape().runAsync()
        var badalbums = [BadAlbum]()
        for item in shapes {
            let badalbum = try await GetOneBadAlbum().runAsync(number: item.id)
            print("bad album: \(badalbum.title), icons: \(badalbum.icons.count)")
            badalbums.append(badalbum)
        }
        return badalbums
    }
}

public struct SaveAndRead {
    
    let foldername = "Albums"
    
    var folder: Folder {
        get { Folders().getLocaleFolder(foldername: foldername)! }
    }
    
    public init() {}
    
    func save(model: [BadAlbum]) {
        let file = try! folder.createFile(named: "allalbums.json")
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(model)
        try! file.write(data)
    }
    public func read() -> [BadAlbum]? {
        guard let file = try? folder.file(named: "allalbums.json") else {
            return nil }
        guard let data = try? file.read() else {
            return nil
        }
        let model = try? JSONDecoder().decode([BadAlbum].self, from: data)
        return model
    }
}

public struct GetAlbumsShape {
    let filename = "albumshape.json"
    
    var folder: Folder { Folders().root! }
    
    public init() {}
    
    public func getFromDiskSync() throws -> [Album]? {
        guard let file = try? folder.file(named: filename) else { return nil}
        guard let data = try? file.read() else { return nil }
        let model = try JSONDecoder().decode([Album].self, from: data)
        return model
    }
    
    public func getFromDisk() async throws -> [Album]? {
        guard let file = try? folder.file(named: filename) else { return nil}
        guard let data = try? file.read() else { return nil }
        let model = try JSONDecoder().decode([Album].self, from: data)
        return model
    }
    public func runAsync() async throws -> [Album] {
        if let model = try await getFromDisk() {
            return model
        } else {
            let (data, _) = try await URLSession.shared.data(for: Request().makeRequest(distanation: .iconPacks(nil)))
            let model = try JSONDecoder().decode([Album].self, from: data)
            await save(model: model)
            return model
        }
    }
    func save(model: [Album]) async {
        let file = try! folder.createFile(named: filename)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(model)
        try! file.write(data)
    }
}

struct GetOneIcon {
    func runAsync() async throws {
        let (data, _) = try await URLSession.shared.data(for: Request().makeRequest(distanation: .icons(nil)))
        let model = try JSONDecoder().decode(BadIcon.self, from: data)
        print("model: \(model)")
    }
}

struct GetOneBadAlbum {
    func runAsync(number: Int?) async throws -> BadAlbum {
        let (data, _) = try await URLSession.shared.data(for: Request().makeRequest(distanation: .iconPacks(number)))
        let model = try JSONDecoder().decode(BadAlbum.self, from: data)
        return model
    }
}


struct Explorer {
    func runAsync() async throws {
        let d1 = try await URLSession.shared.data(for: Request().makeRequest(distanation: .icons(5)))
        let z = try! JSONSerialization.jsonObject(with: d1.0, options: .fragmentsAllowed)
        print(z)
    }
}

