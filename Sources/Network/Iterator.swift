import Files
import Foundation

public struct AutomaticIterator: AsyncIteratorProtocol {
    
    let foldername = "Albums"
    var folder: Folder { Folders().getLocaleFolder(foldername: foldername)! }
    let eachLenth: Int?
    
    var shapes: Set<Album>
    var currentAlbumIterator: IndexingIterator<Array<BadIcon>>?
    
    
    mutating public func next() async -> BadIcon? {
        if currentAlbumIterator != nil {
            if let next = currentAlbumIterator!.next() {
                return next
            } else {
                if let nextalbum = await nextAlbum() {
                    currentAlbumIterator = nextalbum.icons.makeIterator()
                    return currentAlbumIterator!.next()
                } else {
                    return nil
                }
            }
        } else {
            if let nextalbum = await nextAlbum() {
                currentAlbumIterator = nextalbum.icons.makeIterator()
                return currentAlbumIterator!.next()
            } else {
                return nil
            }
        }
    }
    
    mutating func nextAlbum() async -> BadAlbum? {
        guard let shape = shapes.sorted().first else {
            return nil
        }
        shapes.remove(shape)
        var badAlbum: BadAlbum
        if let badalbum = await readBadAlbum(album: shape) {
            badAlbum = badalbum
        } else {
            do {
                var album = try await GetOneBadAlbum().runAsync(number: shape.id)
                album.icons = album.icons.sorted()
                badAlbum = album
                try await save(badAlbum: badAlbum)
            } catch {
                return nil // Bug
            }
        }
        if let length = eachLenth {
            badAlbum.icons = Array(badAlbum.icons.prefix(length))
        }
        return badAlbum
    }
    
    func readBadAlbum(album: Album) async -> BadAlbum? {
        guard let file = try? folder.file(named: album.name) else { return nil }
        guard let data = try? file.read() else { return nil }
        let model = try? JSONDecoder().decode(BadAlbum.self, from: data)
        return model
    }
    
    func save(badAlbum: BadAlbum) async throws {
        let file = try folder.createFile(named: badAlbum.title)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(badAlbum)
        try! file.write(data)
    }
    
}

public struct Automatic: AsyncSequence {
    public typealias AsyncIterator = AutomaticIterator
    public typealias Element = BadIcon
    
    let shapes: Set<Album>
    let eachLenth: Int?
    
    public init(eachLenth: Int?) async throws {
        let shapes = try await GetAlbumsShape().runAsync()
        self.shapes = Set(shapes)
        self.eachLenth = eachLenth
    }
    public func makeAsyncIterator() -> AutomaticIterator {
        AutomaticIterator(eachLenth: eachLenth, shapes: shapes)
    }
}
