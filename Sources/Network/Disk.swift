import Files


struct Folders {
    
    var root: Folder? {
        let rootname = "Iconica"
        if let folder = try? Folder.documents?.subfolder(named: rootname) {
            return folder
        } else {
            let folder = try? Folder.documents?.createSubfolder(named: rootname)
            return folder
        }
    }
    
    func getLocaleFolder(foldername: String) -> Folder? {
        if let folder = try? root?.subfolder(named: foldername) {
            return folder
        } else {
            let folder = try? root?.createSubfolder(named: foldername)
            return folder
        }
    }
}

public struct LoadAtOnce {
    
    public init() {}
    
    public func load() {
        
    }
    
}
