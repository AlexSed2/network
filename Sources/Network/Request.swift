import Foundation

struct Request {
    enum Distanation {
        case iconPacks(Int?)
        case icons(Int?)
        var path: String {
            switch self {
            case .iconPacks(let n):
                var suffix = ""
                if let n = n { suffix = "/\(n)" }
                return "icon_packs" + suffix
            case .icons(let n):
                var suffix = ""
                if let n = n { suffix = "/\(n)" }
                return "icons" + suffix
            }
        }
    }
    func makeRequest(distanation: Distanation) -> URLRequest {
        let token = "2c375e8d-9a19-4033-87ed-cd958b29769a"
        var request = URLRequest(url: buildURL(distanation: distanation)!)
        request.addValue(token, forHTTPHeaderField: "Authorization")
        return request
    }
    func buildURL(distanation: Distanation) -> URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.path = "/api/v2/" + distanation.path
        components.host = "ikonika.app"
        return components.url
    }
}
