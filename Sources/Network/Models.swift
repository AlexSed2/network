import Foundation

public struct BadIcon: Codable, Equatable, Hashable, CustomStringConvertible, Comparable {
    
    public let id: Int
    public let title: String
    public let image: String
    public let pack: Int
    public var description: String { "icon id: \(id), pack: \(pack), name: \(title)" }
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case image
        case pack = "v2_icon_pack_id"
    }
    
    
    public static func == (lhs: BadIcon, rhs: BadIcon) -> Bool {
        lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
         hasher.combine(id)
    }
    public static func < (lhs: BadIcon, rhs: BadIcon) -> Bool {
        lhs.id < rhs.id
    }
}

public struct BadAlbum: Codable {
    public let id: Int
    public let title: String
    public var icons: [BadIcon]
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case icons = "v2_icons"
    }
}

public struct Album: Equatable, Hashable, Codable, Comparable {
    
    public let id: Int
    public let name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name = "title"
    }
    
    public static func == (lhs: Album, rhs: Album) -> Bool {
        lhs.id == rhs.id
    }
    public static func < (lhs: Album, rhs: Album) -> Bool {
        lhs.id < rhs.id
    }
    public func hash(into hasher: inout Hasher) {
         hasher.combine(id)
    }
}

struct AlbumShape {
    var albums: Set<Album>
}

